use std::io::Cursor;
use std::{fs, path::Path, sync::Arc};

use archivist::Archivist;
use archivist::{store::GzStore, tree::DateTimeDirTree};
use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use tempdir::TempDir;
use tokio::runtime::Runtime;
use tokio::task::JoinSet;

const DATA_DIR: &str = "./tests/data";

fn create_archive(b: &mut Criterion) {
    let rt = Runtime::new().unwrap();
    let dir = TempDir::new("archivist").expect("Create tmp dir");
    b.bench_function("create archive", |b| {
        b.to_async(&rt)
            .iter(|| Archivist::new(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1))
    });
}

fn open_archive(b: &mut Criterion) {
    let rt = Runtime::new().unwrap();
    let dir = TempDir::new("archivist").expect("Create tmp dir");

    let _a = rt
        .handle()
        .block_on(async {
            Archivist::new(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1).await
        })
        .unwrap();

    b.bench_function("open archive", |b| {
        b.to_async(&rt)
            .iter(|| Archivist::open(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1))
    });
}

fn add_string(b: &mut Criterion) {
    let rt = Runtime::new().unwrap();
    let handle = rt.handle();

    let path = Path::new(DATA_DIR).join("words.txt");
    let dir = TempDir::new("archivist").expect("Create tmp dir");
    let a = Arc::new(handle.block_on(async {
        Archivist::new(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1)
            .await
            .unwrap()
    }));
    let s = Arc::new(fs::read_to_string(path).unwrap());
    b.bench_with_input(BenchmarkId::new("add", "StringStore"), &a, |b, a| {
        b.to_async(&rt).iter(|| {
            let a = a.clone();
            let s = s.clone();

            async move {
                a.add("words.txt", &*s).await.unwrap();
            }
        });
    });
}

fn add_string_gz(b: &mut Criterion) {
    let rt = Runtime::new().unwrap();
    let handle = rt.handle();

    let path = Path::new(DATA_DIR).join("words.txt");
    let dir = TempDir::new("archivist").expect("Create tmp dir");
    let a = Arc::new(handle.block_on(async {
        Archivist::new(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1)
            .await
            .unwrap()
    }));
    let s = Arc::new(fs::read_to_string(path).unwrap());

    b.bench_with_input(BenchmarkId::new("add", "GzStore"), &a, |b, a| {
        b.to_async(&rt).iter(|| {
            let a = a.clone();
            let s = s.clone();

            async move {
                let ss = GzStore::new(Cursor::new(&*s));
                a.add("words.txt.gz", &ss).await.unwrap();
            }
        });
    });
}

fn add_image(b: &mut Criterion) {
    let rt = Runtime::new().unwrap();
    let handle = rt.handle();

    let path = Path::new(DATA_DIR).join("lena.tiff");
    let dir = TempDir::new("archivist").expect("Create tmp dir");
    let a = Arc::new(handle.block_on(async {
        Archivist::new(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1)
            .await
            .unwrap()
    }));
    let s = Arc::new(image::open(path).unwrap());

    [
        ("JpgStore", "lena.jpg"),
        ("PngStore", "lena.png"),
        ("TiffStore", "lena.tiff"),
        ("WebpStore", "lena.webp"),
        ("BmpStore", "lena.bmp"),
    ]
    .iter()
    .for_each(|(name, file)| {
        b.bench_with_input(BenchmarkId::new("add", name), &a, |b, a| {
            b.to_async(&rt).iter(|| {
                let a = a.clone();
                let s = s.clone();

                async move {
                    a.add(file, &*s).await.unwrap();
                }
            });
        });
    })
}

fn add_image_parallel(b: &mut Criterion) {
    let rt = Runtime::new().unwrap();
    let handle = rt.handle();

    let path = Path::new(DATA_DIR).join("lena.tiff");
    let dir = TempDir::new("archivist").expect("Create tmp dir");
    let a = Arc::new(handle.block_on(async {
        Archivist::new(dir.path().to_str().unwrap(), DateTimeDirTree::default(), 1)
            .await
            .unwrap()
    }));
    let s = Arc::new(image::open(path).unwrap());

    b.bench_with_input(BenchmarkId::new("add", "PngStoreParallel"), &a, |b, a| {
        b.to_async(&rt).iter(|| {
            let a = a.clone();
            let s = s.clone();

            async move {
                let mut set = JoinSet::new();
                for _ in 0..100 {
                    let a = a.clone();
                    let s = s.clone();
                    set.spawn(async move { a.add("lena.png", &*s).await });
                }
                set.join_all().await;
            }
        });
    });
}

criterion_group!(
    benches,
    create_archive,
    open_archive,
    add_string,
    add_string_gz,
    add_image,
    add_image_parallel
);
criterion_main!(benches);
