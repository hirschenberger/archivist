use std::{fs, path::Path, time::Duration};

use anyhow::Result;
use archivist::{tree::DateTimeDirTree, Archivist};
use metrics_exporter_prometheus::PrometheusBuilder;
use tempdir::TempDir;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();

    let builder = PrometheusBuilder::new();
    builder.install()?;

    let image_path = Path::new("./tests/data").join("words.txt");
    let dir = TempDir::new("archivist").expect("Create tmp dir");
    let a = Archivist::new(
        dir.path().to_str().unwrap(),
        DateTimeDirTree::default(),
        10000,
    )
    .await?;
    let s = fs::read_to_string(image_path.clone()).unwrap();
    for _ in 0..1000000 {
        let res = a.add("string.txt", &s).await;
        tokio::time::sleep(Duration::from_millis(100)).await;
        assert!(res.is_ok(), "{:?}", res);
    }

    Ok(())
}
