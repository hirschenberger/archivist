# ![](logo_small.png) Archivist

[![Crates.io Version](https://img.shields.io/crates/v/archivist)](https://crates.io/crates/archivist)
[![](https://gitlab.com/hirschenberger/archivist/badges/master/pipeline.svg )](https://gitlab.com/hirschenberger/archivist/-/pipelines/latest)
[![Crates.io License](https://img.shields.io/crates/l/archivist)](LICENSE)
[![docs.rs](https://img.shields.io/docsrs/archivist)](https://docs.rs/archivist/0.1.0/archivist/)

Store files in a time or index based directory hierarchy, automatically deleting the oldest files if the
size limit is reached.

It's using an efficient kv-store database for persistency and performance reasons.

```rust
    // Store at max 1GB of files
    let a = Archivist::new("/tmp/archive", DateTimeTree::default(), 1000).await?;
   
    // Our humble file that we want to keep, but may lose interest after some time 
    let mut f = fs::File::open("/tmp/my_words.txt").unwrap();

    // crate a store object for file compression
    let ss = GzStore::new(Cursor::new(f.clone()));

    // add the file to the archive
    a.add("words.txt.gz", &ss).await?;
```

Copyright 2022-2024, Falco Hirschenberger <falco.hirschenberger@gmail.com>


