use anyhow::{Context, Result};
use flate2::{read::GzEncoder, Compression};
use image::ImageFormat;
use std::{
    cell::RefCell,
    io::{BufRead, Cursor, Read, Seek},
    path::Path,
    pin::Pin,
};
use tokio::io;

pub trait Store {
    fn store(&self, path: &Path) -> Result<Pin<Box<dyn io::AsyncBufRead + Send>>>;
}

impl Store for String {
    fn store(&self, _path: &Path) -> Result<Pin<Box<dyn io::AsyncBufRead + Send>>> {
        Ok(Box::pin(Cursor::new(self.clone())))
    }
}

impl Store for image::DynamicImage {
    fn store(&self, path: &Path) -> Result<Pin<Box<dyn io::AsyncBufRead + Send>>> {
        let v = Vec::new();
        let mut buff = Cursor::new(v);
        self.write_to(
            &mut buff,
            ImageFormat::from_path(path).with_context(|| {
                format!(
                    "Can't determine an image format from path: {}",
                    path.display()
                )
            })?,
        )
        .with_context(|| format!("Can't encode image for path: {}", path.display()))?;

        // vital to rewind the cursor here to make the reader start from the beginning
        buff.rewind()?;
        Ok(Box::pin(buff))
    }
}

pub struct GzStore<R: BufRead>(RefCell<GzEncoder<R>>);

impl<R: BufRead> GzStore<R> {
    pub fn new(r: R) -> Self {
        GzStore(RefCell::new(GzEncoder::new(r, Compression::fast())))
    }
}

impl<R: BufRead> Store for GzStore<R> {
    fn store(&self, _path: &Path) -> Result<Pin<Box<dyn io::AsyncBufRead + Send>>> {
        let mut buff = Vec::new();
        self.0.borrow_mut().read_to_end(&mut buff)?;
        Ok(Box::pin(Cursor::new(buff)))
    }
}
